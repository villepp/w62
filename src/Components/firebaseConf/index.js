import { initializeApp } from "firebase/app";
import { getDatabase } from "firebase/database";

function StartFirebase() {
  const firebaseConfig = {
    apiKey: "AIzaSyBizTsJqhW5WKazYqhhRyKLlgQxcusi4DE",
    authDomain: "reacttesting-6e6fc.firebaseapp.com",
    databaseURL:
      "https://reacttesting-6e6fc-default-rtdb.europe-west1.firebasedatabase.app",
    projectId: "reacttesting-6e6fc",
    storageBucket: "reacttesting-6e6fc.appspot.com",
    messagingSenderId: "547357135634",
    appId: "1:547357135634:web:e347e859140935924b19cb",
    measurementId: "G-TNNMMBE0KT",
  };

  const app = initializeApp(firebaseConfig);
  return getDatabase(app);
}

export default StartFirebase;